class UsersController < ApplicationController

  before_filter :authenticate_user!

  @@user = "Users"

  def index
    @users = User.all
    if current_user.nameProfile_id != 0
      @per = Profile.where("nameProfile_id = ?", current_user.nameProfile_id)
      @per.each do |permisos|
        if permisos.name == @@user
          @uno = permisos.name
          @@crear = permisos.crear
          @@editar = permisos.editar
          @@leer = permisos.leer
          @@eliminar = permisos.eliminar

          @crear = permisos.crear
          @editar = permisos.editar
          @leer = permisos.leer
          @eliminar = permisos.eliminar
        end
      end
      if ((current_user.nameProfile_id == 1)||(@@leer == 2))
        @users = User.all
      else
        redirect_to home_index_path, :alert => "Ask for Permissions!"
      end
    else
      redirect_to home_index_path, :alert => "Ask for Permissions!"
    end
  end

  def show

    @crear = @@crear
    @editar = @@editar
    @leer = @@leer
    @eliminar = @@eliminar

    if current_user.nameProfile_id != 0
      if ((current_user.nameProfile_id == 1) || (@@leer == 2))
        @user = User.find(params[:id])
      else
        redirect_to home_index_path, :alert => "Ask for Permissions!"
      end
    else
      redirect_to home_index_path, :alert => "Ask for Permissions!"
    end
  end


  def update
    @user = User.find(params[:id])
    if params[:user][:nameProfile_id].present?
      if @user.update_attributes(secure_params).present?
        redirect_to users_path, :notice => "User updated."
        ChangePerfil.permisos_otorgados(@user).deliver
      else
        redirect_to users_path, :alert => "Unable to update user."
      end
    else
      redirect_to users_path, :alert => "You have not selected a profile."
    end
  end

  def permissions
    @user = current_user
    @admin = User.where("nameProfile_id = 1")
    @admin.each do |f|
      @email = f.email
      ChangePerfil.mandar_permisos(@user, @email).deliver
    end
    redirect_to home_index_path, :notice => "We got an email with the granted permissions."
  end

  def destroy
    user = User.find(params[:id])
    user.destroy
    redirect_to users_path, :notice => "User deleted."
  end

  private

  def secure_params
    params.require(:user).permit(:nameProfile_id)
  end


end

class Sale < ApplicationRecord

  has_many :products
  belongs_to :shop
end

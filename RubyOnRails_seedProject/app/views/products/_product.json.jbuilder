json.extract! product, :id, :sale_id, :product_name, :amount, :quantity, :unity, :created_at, :updated_at
json.url product_url(product, format: :json)

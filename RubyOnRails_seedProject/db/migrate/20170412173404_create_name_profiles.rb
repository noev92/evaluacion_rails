class CreateNameProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :name_profiles do |t|
      t.string :name
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end
  end
end
